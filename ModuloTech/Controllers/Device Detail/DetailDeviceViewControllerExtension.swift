//
//  DetailDeviceViewControllerExtension.swift
//  ModuloTech
//
//  Created by HieuH on 17/06/2021.
//

import Foundation
import UIKit

extension DetailDeviceViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if device?.productType != FilterType.heaters.displayName {
            return 101
        } else {
            return temperatures.count
        }
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if device?.productType != FilterType.heaters.displayName {
            return "\(row)"
        } else {
            return temperatures[row]
        }
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if device?.productType != FilterType.heaters.displayName {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "didUpdateDevice"), object: nil, userInfo: ["id": device?.id as Any, "intensity": row as Any])
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "didUpdateDevice"), object: nil, userInfo: ["id": device?.id as Any, "temperature": temperatures[row].dropLast() as Any])
        }
    }
}
