//
//  DetailDeviceViewController.swift
//  ModuloTech
//
//  Created by HieuH on 17/06/2021.
//

import UIKit

class DetailDeviceViewController: UIViewController {

    let inputTextIdentify = "InputTextCell"
    let inputTextDoubleIdentify = "InputTextDoubleCell"
    var mainTableView = UITableView()
    var positionLabel = UILabel()
    var rollerShutterSlider = UISlider()

    var screenWidth: CGFloat = 0
    var device: Device?
    let temperatures: [String] = ["7°","12°","17°","22°","27°","28°"]

    override func viewDidLoad() {
        super.viewDidLoad()
        configMainContent()
        setupLayout()
        setupHeaderView()
        configMainContent()
    }

    func setupLayout() {
        navigationController?.navigationBar.isHidden = true
        screenWidth = UIScreen.main.bounds.width
        self.view.backgroundColor = .white
    }

    func setupHeaderView() {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 80))
        self.view.addSubview(headerView)

        let backImage = UIImageView(frame: CGRect(x: 20, y: 48, width: 32, height: 32))
        headerView.addSubview(backImage)
        backImage.image = #imageLiteral(resourceName: "icon_close")
        backImage.isUserInteractionEnabled = true
        let tapBack = UITapGestureRecognizer(target: self, action: #selector(self.handleTapBack(_:)))
        backImage.addGestureRecognizer(tapBack)

        let mainLabel = UILabel()
        headerView.addSubview(mainLabel)
        mainLabel.textAlignment = .center
        mainLabel.font = UIFont.systemFont(ofSize: 24)
        mainLabel.text = device?.deviceName
        mainLabel.translatesAutoresizingMaskIntoConstraints = false
        mainLabel.leadingAnchor.constraint(equalTo: backImage.trailingAnchor, constant: 4).isActive = true
        mainLabel.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -36).isActive = true
        mainLabel.bottomAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
    }

    @objc
    func handleTapBack(_ sender: UITapGestureRecognizer?) {
        self.dismiss(animated: true, completion: nil)
    }

    func configMainContent() {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(imageView)


        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 100).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 250).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 250).isActive = true
        imageView.contentMode = .scaleAspectFill

        let mainStackView = UIStackView()
        mainStackView.axis = .vertical
        mainStackView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(mainStackView)

        mainStackView.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 24).isActive = true
        mainStackView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 24).isActive = true
        mainStackView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -24).isActive = true
        mainStackView.spacing = 12
        mainStackView.distribution = .fillProportionally

        switch device?.productType {
        case FilterType.lights.displayName:
            if let mode = device?.mode {
                mainStackView.addArrangedSubview(addSwitchView(isOn: mode == "ON"))
            }
            if device?.intensity != nil {
                mainStackView.addArrangedSubview(addPickerView())
            }
            imageView.image = #imageLiteral(resourceName: "ic_lights")
        case FilterType.rollerShutter.displayName:
            imageView.image = #imageLiteral(resourceName: "roller_shutter")
            addSlider(to: imageView)
        case FilterType.heaters.displayName:
            if let mode = device?.mode {
                mainStackView.addArrangedSubview(addSwitchView(isOn: mode == "ON"))
            }
            if device?.temperature != nil {
                mainStackView.addArrangedSubview(addPickerView())
            }
            imageView.image = #imageLiteral(resourceName: "heaters")
        default:
            imageView.image = #imageLiteral(resourceName: "ic_lights")
        }
    }

    // Picker View Choose Intensity
    func addPickerView() -> UIView {
        let childView = UIView()
        childView.backgroundColor = UIColor.init(hexString: "#E5E5E5")
        childView.layer.cornerRadius = 6

        let topLabel = UILabel()
        topLabel.translatesAutoresizingMaskIntoConstraints = false
        childView.addSubview(topLabel)

        topLabel.text = device?.productType == FilterType.heaters.displayName ? Localization.string(.temperature_title) : Localization.string(.intensity_title)
        topLabel.topAnchor.constraint(equalTo: childView.topAnchor, constant: 4).isActive = true
        topLabel.centerXAnchor.constraint(equalTo: childView.centerXAnchor).isActive = true

        let pickerView = UIPickerView()
        pickerView.translatesAutoresizingMaskIntoConstraints = false
        childView.addSubview(pickerView)

        pickerView.dataSource = self
        pickerView.delegate = self
        if device?.productType != FilterType.heaters.displayName {
            pickerView.selectRow(Int(device?.intensity ?? 0), inComponent: 0, animated: true)
        }

        pickerView.centerXAnchor.constraint(equalTo: childView.centerXAnchor).isActive = true
        pickerView.topAnchor.constraint(equalTo: topLabel.bottomAnchor, constant: 4).isActive = true
        pickerView.bottomAnchor.constraint(equalTo: childView.bottomAnchor, constant: -4).isActive = true
        pickerView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        pickerView.heightAnchor.constraint(equalToConstant: screenWidth - 48).isActive = true

        return childView
    }

    // Slider for Roller Shutter
    func addSlider(to childView: UIView) {
        let customeThumb = UIImage.circle(diameter: 24, color: UIColor.init(hexString: "#565656"), borderWidth: 6, borderColor: .white)
        rollerShutterSlider.maximumValue = 100
        rollerShutterSlider.minimumValue = 0
        rollerShutterSlider.value = Float(device?.position ?? 0)
        rollerShutterSlider.maximumTrackTintColor = UIColor.init(hexString: "#ECF0F1")
        rollerShutterSlider.minimumTrackTintColor = UIColor.init(hexString: "#A88063")
        rollerShutterSlider.transform = CGAffineTransform(rotationAngle: -(.pi / 2))
        rollerShutterSlider.setThumbImage(customeThumb, for: .normal)
        rollerShutterSlider.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(rollerShutterSlider)

        rollerShutterSlider.addTarget(self, action: #selector(changeSliderValue), for: .valueChanged)
        rollerShutterSlider.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        rollerShutterSlider.topAnchor.constraint(equalTo: childView.bottomAnchor, constant: 74).isActive = true
        rollerShutterSlider.heightAnchor.constraint(equalToConstant: 50).isActive = true
        rollerShutterSlider.widthAnchor.constraint(equalToConstant: 200).isActive = true

        let topLabel = UILabel()
        topLabel.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(topLabel)

        topLabel.text = "100"
        topLabel.topAnchor.constraint(equalTo: childView.bottomAnchor, constant: 0).isActive = true
        topLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 36).isActive = true

        positionLabel.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(positionLabel)

        positionLabel.text = "\(Localization.string(.position_title)): \(device?.position ?? 0)"
        positionLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 24).isActive = true
        positionLabel.topAnchor.constraint(equalTo: childView.bottomAnchor, constant: 100).isActive = true

        let bottomLabel = UILabel()
        bottomLabel.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(bottomLabel)

        bottomLabel.text = "0"
        bottomLabel.topAnchor.constraint(equalTo: childView.bottomAnchor, constant: 180).isActive = true
        bottomLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 36).isActive = true
    }

    @objc
    func changeSliderValue(sender: UISlider) {
        let roundedValue = round(sender.value / 10) * 10
        sender.value = roundedValue
        positionLabel.text = "\(Localization.string(.position_title)): \(Int(rollerShutterSlider.value))"
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "didUpdateDevice"), object: nil, userInfo: ["id": device?.id as Any, "position": roundedValue as Any])
    }

    // Switch for ON/OFF Mode
    func addSwitchView(isOn: Bool) -> UIView {
        let mainView = UIView()
        mainView.backgroundColor = UIColor.init(hexString: "#E5E5E5")
        mainView.layer.cornerRadius = 6

        let topLabel = UILabel()
        topLabel.translatesAutoresizingMaskIntoConstraints = false
        mainView.addSubview(topLabel)

        topLabel.text = Localization.string(.mode_title)
        topLabel.topAnchor.constraint(equalTo: mainView.topAnchor, constant: 4).isActive = true
        topLabel.centerXAnchor.constraint(equalTo: mainView.centerXAnchor).isActive = true

        let mainSwitch = UISwitch()
        mainSwitch.tintColor = .gray
        mainSwitch.backgroundColor = .gray
        mainSwitch.layer.cornerRadius = 16
        mainSwitch.onTintColor = .orange
        mainSwitch.isOn = isOn
        mainSwitch.translatesAutoresizingMaskIntoConstraints = false
        mainView.addSubview(mainSwitch)
        mainSwitch.addTarget(self, action: #selector(handleChageMode), for: .valueChanged)

        mainSwitch.centerXAnchor.constraint(equalTo: mainView.centerXAnchor).isActive = true
        mainSwitch.topAnchor.constraint(equalTo: topLabel.bottomAnchor, constant: 4).isActive = true
        mainSwitch.bottomAnchor.constraint(equalTo: mainView.bottomAnchor, constant: -4).isActive = true
        return mainView
    }

    @objc
    func handleChageMode(_ sender: UISwitch) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "didUpdateDevice"), object: nil, userInfo: ["id": device?.id as Any, "mode": sender.isOn as Any])
    }
}
