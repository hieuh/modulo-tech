//
//  ProfileDetailViewController.swift
//  ModuloTech
//
//  Created by HieuH on 17/06/2021.
//

import UIKit

class ProfileDetailViewController: UIViewController {

    let inputTextIdentify = "InputTextCell"
    let inputTextDoubleIdentify = "InputTextDoubleCell"
    var mainTableView = UITableView()
    var screenWidth: CGFloat = 0
    var screenHeight: CGFloat = 0
    var isChanged: Bool = false

    var userInfor: UserInfor?
    var newUserInfor: UserInfor?

    override func viewDidLoad() {
        super.viewDidLoad()

        newUserInfor = userInfor
        setupLayout()
        setupHeaderView()
        addTableView()
        setupContentView()

        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    @objc
    func keyboardWillShow (notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            mainTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
        }
    }

    @objc
    func keyboardWillHide(notification: NSNotification) {
        mainTableView.contentInset = .zero
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    func setupLayout() {
        navigationController?.navigationBar.isHidden = true
        screenWidth = UIScreen.main.bounds.width
        screenHeight = UIScreen.main.bounds.height
        self.view.backgroundColor = .white
    }

    func addTableView() {
        let inputTextNib = UINib(nibName: inputTextIdentify, bundle: nil)
        let inputTextDoubleNib = UINib(nibName: inputTextDoubleIdentify, bundle: nil)

        mainTableView = UITableView(frame: CGRect(x: 0, y: 80, width: UIScreen.main.bounds.width, height: screenHeight - 153), style: .grouped)
        mainTableView.backgroundColor = .white
        mainTableView.separatorStyle = .none
        mainTableView.register(inputTextNib, forCellReuseIdentifier: inputTextIdentify)
        mainTableView.register(inputTextDoubleNib, forCellReuseIdentifier: inputTextDoubleIdentify)

        mainTableView.delegate = self
        mainTableView.dataSource = self

        self.view.addSubview(mainTableView)

    }

    func setupHeaderView() {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 80))
        self.view.addSubview(headerView)

        let backImage = UIImageView(frame: CGRect(x: 20, y: 48, width: 32, height: 32))
        headerView.addSubview(backImage)
        backImage.image = #imageLiteral(resourceName: "icon_close")
        backImage.isUserInteractionEnabled = true
        let tapBack = UITapGestureRecognizer(target: self, action: #selector(self.handleTapBack(_:)))
        backImage.addGestureRecognizer(tapBack)

        let mainLabel = UILabel()
        headerView.addSubview(mainLabel)
        mainLabel.textAlignment = .center
        mainLabel.font = UIFont.systemFont(ofSize: 24)
        mainLabel.text = Localization.string(.edit_profile_title)
        mainLabel.translatesAutoresizingMaskIntoConstraints = false
        mainLabel.leadingAnchor.constraint(equalTo: backImage.trailingAnchor, constant: 4).isActive = true
        mainLabel.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -36).isActive = true
        mainLabel.bottomAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
    }

    @objc
    func handleTapBack(_ sender: UITapGestureRecognizer?) {
        self.dismiss(animated: true, completion: nil)
    }

    func setupContentView() {
        let saveButton = UIButton(frame: CGRect(x: 20, y: screenHeight - 72, width: screenWidth - 40, height: 44))
        self.view.addSubview(saveButton)

        saveButton.addTarget(self, action: #selector(handleTapDone), for: .touchUpInside)
        saveButton.layer.cornerRadius = 22
        saveButton.setTitle(Localization.string(.save_changes_title), for: .normal)
        saveButton.backgroundColor = UIColor.init(hexString: "#31B9CC")
    }

    @objc
    func handleTapDone(_ sender: UITapGestureRecognizer?) {
        if isChanged, validateObject() {
            if isValidEmail(newUserInfor?.email ?? "") {
                let message = Localization.string(.save_changes_success_msg)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "didUpdateInfor"), object: nil, userInfo: ["newUserInfor": newUserInfor as Any])
                let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
                self.present(alert, animated: true)

                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                    alert.dismiss(animated: true, completion: { [weak self] in
                        self?.dismiss(animated: true, completion: nil)
                    })
                }
            } else {
                showPopupWarning(message: Localization.string(.email_not_correct_msg))
            }
        } else {
            showPopupWarning()
        }
    }

    func showPopupWarning(message: String? = nil) {
        let viewController = PopupCommonViewController()
        viewController.alertType = .warning
        viewController.message = message?.isEmpty ?? true ? Localization.string(.can_not_empty_msg) : message
        viewController.modalTransitionStyle = .coverVertical
        viewController.modalPresentationStyle = .overFullScreen
        self.present(viewController, animated: true, completion: nil)
    }

    func validateObject() -> Bool {
        if isChanged {
            if let user = newUserInfor {
                if user.firstName?.isEmpty == true {
                    return false
                }
                if user.lastName?.isEmpty == true {
                    return false
                }
                if user.email?.isEmpty == true {
                    return false
                }
                if user.address?.street?.isEmpty == true {
                    return false
                }
                if user.address?.streetCode?.isEmpty == true {
                    return false
                }
                if user.address?.city?.isEmpty == true {
                    return false
                }
                if user.address?.postalCode == nil {
                    return false
                }
                if user.address?.country?.isEmpty == true {
                    return false
                }
                return true
            }
            return false
        }
        return true
    }

    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }

}

extension ProfileDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ProfileDetailType.allCases.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let type = ProfileDetailType(rawValue: indexPath.row)
        switch type {
        case .email, .dateOfBirth, .country:
            let cell: InputTextCell = tableView.dequeueReusableCell(withIdentifier: inputTextIdentify, for: indexPath) as! InputTextCell
            cell.delegate = self
            cell.configData(type: type ?? .firstName, userInfor: userInfor)
            return cell
        default:
            let cell: InputTextDoubleCell = tableView.dequeueReusableCell(withIdentifier: inputTextDoubleIdentify, for: indexPath) as! InputTextDoubleCell
            cell.delegate = self
            cell.configData(type: type ?? .firstName, userInfor: userInfor)
            return cell
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "ic_profile")
        imageView.layer.cornerRadius = 50
        imageView.translatesAutoresizingMaskIntoConstraints = false
        headerView.addSubview(imageView)

        imageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        imageView.centerXAnchor.constraint(equalTo: headerView.centerXAnchor).isActive = true
        imageView.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true

        return headerView
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 164
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
}

extension ProfileDetailViewController: InputTextCellDelegate {
    func didTapOnCalendar() {
        let viewController = ChooseDatePickerViewController()
        viewController.modalPresentationStyle = .overCurrentContext
        viewController.modalTransitionStyle = .crossDissolve
        viewController.currentDate = Date(timeIntervalSince1970: Double((userInfor?.birthDate ?? 0)) / 1000.0)
        viewController.didChangeDate = { [weak self] date in
            guard let self = self else {
                return
            }
            self.userInfor?.birthDate = Int64((date.timeIntervalSince1970 * 1000.0).rounded())
            self.newUserInfor?.birthDate = Int64((date.timeIntervalSince1970 * 1000.0).rounded())
            self.mainTableView.reloadData()
        }
        self.present(viewController, animated: true)

    }

    func didChangeValue(mainType: ProfileDetailType, childType: ChildProfileType?, value: String) {
        isChanged = true
        if let childType = childType {
            switch childType {
            case .lastName:
                newUserInfor?.lastName = value
            case .postalCode:
                newUserInfor?.address?.postalCode = Int64(value)
            default:
                newUserInfor?.address?.streetCode = value
            }
            return
        }
        switch mainType {
        case .firstName:
            newUserInfor?.firstName = value
        case .email:
            newUserInfor?.email = value
        case .street:
            newUserInfor?.address?.street = value
        case .city:
            newUserInfor?.address?.city = value
        case .country:
            newUserInfor?.address?.country = value
        default:
            break
        }
    }
}
