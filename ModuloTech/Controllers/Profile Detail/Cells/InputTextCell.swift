//
//  InputTextCell.swift
//  ModuloTech
//
//  Created by HieuH on 17/06/2021.
//

import UIKit

protocol InputTextCellDelegate: NSObjectProtocol {
    func didTapOnCalendar()
    func didChangeValue(mainType: ProfileDetailType, childType: ChildProfileType?, value: String)
}

class InputTextCell: UITableViewCell {
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var inputTextField: UITextField!
    @IBOutlet private weak var calendarImageView: UIImageView!
    @IBOutlet private weak var heightOfImageViewContraint: NSLayoutConstraint!

    weak var delegate: InputTextCellDelegate?
    var currentType: ProfileDetailType = .firstName

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none

        containerView.layer.cornerRadius = 10
        inputTextField.delegate = self

        calendarImageView.isUserInteractionEnabled = true
        let tapCalendar = UITapGestureRecognizer(target: self, action: #selector(self.handleTapCalendar(_:)))
        calendarImageView.addGestureRecognizer(tapCalendar)
    }

    @objc
    func handleTapCalendar(_ sender: UITapGestureRecognizer?) {
        delegate?.didTapOnCalendar()
    }

    func configData(type: ProfileDetailType, userInfor: UserInfor?) {
        guard let userInfor = userInfor else {
            return
        }
        currentType = type
        titleLabel.text = type.displayName
        inputTextField.isEnabled = true
        calendarImageView.isHidden = true
        switch type {
        case .email:
            inputTextField.text = userInfor.email
        case .dateOfBirth:
            let date = Date(timeIntervalSince1970: Double((userInfor.birthDate ?? 0)) / 1000.0)
            inputTextField.text = dateToString(date: date)
            calendarImageView.isHidden = false
            inputTextField.isEnabled = false
        case .country:
            inputTextField.text = userInfor.address?.country
        default:
            break
        }
    }

    func dateToString(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let myString = formatter.string(from: date) // string purpose I add here
        let yourDate = formatter.date(from: myString)
        formatter.dateFormat = "dd-MMM-yyyy"
        return formatter.string(from: yourDate!)
    }
}

extension InputTextCell: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
           let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            delegate?.didChangeValue(mainType: self.currentType, childType: nil, value: updatedText)
        }
        return true
    }
}
