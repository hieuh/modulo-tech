//
//  InputTextDoubleCell.swift
//  ModuloTech
//
//  Created by HieuH on 17/06/2021.
//

import UIKit

class InputTextDoubleCell: UITableViewCell {
    @IBOutlet private weak var leftTitleLabel: UILabel!
    @IBOutlet private weak var leftView: UIView!
    @IBOutlet private weak var leftTextField: UITextField!
    @IBOutlet private weak var rightTitleLabel: UILabel!
    @IBOutlet private weak var rightView: UIView!
    @IBOutlet private weak var rightTextField: UITextField!

    weak var delegate: InputTextCellDelegate?
    var currentType: ProfileDetailType = .firstName

    override func awakeFromNib() {
        super.awakeFromNib()

        selectionStyle = .none
        leftView.layer.cornerRadius = 10
        rightView.layer.cornerRadius = 10
        rightTextField.delegate = self
        leftTextField.delegate = self
    }

    func configData(type: ProfileDetailType, userInfor: UserInfor?) {
        guard let userInfor = userInfor else {
            return
        }
        currentType = type
        leftTitleLabel.text = type.displayName
        switch type {
        case .firstName:
            leftTextField.text = userInfor.firstName
            rightTextField.text = userInfor.lastName
            rightTitleLabel.text = ChildProfileType.lastName.displayName
        case .street:
            leftTextField.text = userInfor.address?.street
            rightTextField.text = userInfor.address?.streetCode
            rightTitleLabel.text = ChildProfileType.streetCode.displayName
        case .city:
            leftTextField.text = userInfor.address?.city
            rightTextField.text = "\(userInfor.address?.postalCode ?? 0)"
            rightTitleLabel.text = ChildProfileType.postalCode.displayName
        default:
            leftTitleLabel.text = ""
            break
        }
    }
}

extension InputTextDoubleCell: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
           let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            if textField == rightTextField, currentType == .street {
                delegate?.didChangeValue(mainType: currentType, childType: .streetCode, value: updatedText)
            } else if textField == rightTextField, currentType == .city {
                delegate?.didChangeValue(mainType: currentType, childType: .postalCode, value: updatedText)
            } else if textField == rightTextField, currentType == .firstName {
                delegate?.didChangeValue(mainType: currentType, childType: .lastName, value: updatedText)
            } else {
                delegate?.didChangeValue(mainType: currentType, childType: nil, value: updatedText)
            }
        }
        return true
    }
}
