//
//  ChooseDatePickerViewController.swift
//  ModuloTech
//
//  Created by HieuH on 18/06/2021.
//

import UIKit

class ChooseDatePickerViewController: UIViewController {

    var didChangeDate: ((_ date: Date) -> Void)?
    var currentDate = Date()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .black.withAlphaComponent(0.8)
        setupLayout()

    }

    func setupLayout() {
        let mainStackView = UIStackView()
        mainStackView.backgroundColor = .white
        mainStackView.axis = .vertical
        mainStackView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(mainStackView)

        mainStackView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        mainStackView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        mainStackView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        mainStackView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height * 0.4).isActive = true
        mainStackView.distribution = .fillProportionally

        let doneButton = UIButton()
        doneButton.translatesAutoresizingMaskIntoConstraints = false
        doneButton.backgroundColor = UIColor.init(hexString: "#185ECE")
        doneButton.setTitle(Localization.string(.done_title), for: .normal)
        doneButton.addTarget(self, action: #selector(handleTapDone), for: .touchUpInside)

        doneButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        doneButton.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive = true

        let datePicker = UIDatePicker()
        datePicker.backgroundColor = .white
        datePicker.translatesAutoresizingMaskIntoConstraints = false
        datePicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .compact
        }
        datePicker.setDate(currentDate, animated: true)

        datePicker.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height * 0.35).isActive = true
        datePicker.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive = true

        mainStackView.addArrangedSubview(doneButton)
        mainStackView.addArrangedSubview(datePicker)
    }

    @objc
    func handleTapDone(_ sender: UITapGestureRecognizer?) {
        didChangeDate?(currentDate)
        self.dismiss(animated: true, completion: nil)
    }

    @objc
    func dateChanged(_ sender: UIDatePicker) {
        self.currentDate = sender.date
    }
}
