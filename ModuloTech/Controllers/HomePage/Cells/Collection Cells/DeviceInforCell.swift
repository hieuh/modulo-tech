//
//  DeviceInforCell.swift
//  ModuloTech
//
//  Created by HieuH on 16/06/2021.
//

import UIKit

class DeviceInforCell: UICollectionViewCell {

    @IBOutlet private weak var mainSwitch: UISwitch!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var mainImageView: UIImageView!
    @IBOutlet private weak var mainView: UIView!
    @IBOutlet private weak var deleteButton: UIButton!

    var didSelectRemove: (() -> Void)?
    var didChangeMode: ((_ isOn: Bool) -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        mainSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        mainView.layer.cornerRadius = 16
        mainView.layer.masksToBounds = true

        mainSwitch.tintColor = .gray
        mainSwitch.backgroundColor = .gray
        mainSwitch.layer.cornerRadius = 16
    }

    func setupData(data: Device, didSelectRemove: (() -> Void)?, didChangeMode: ((_ isOn: Bool) -> Void)?) {
        self.didSelectRemove = didSelectRemove
        self.didChangeMode = didChangeMode

        switch data.mode {
        case "OFF":
            mainSwitch.isOn = false
        default:
            mainSwitch.isOn = true
        }
        mainSwitch.isHidden = false

        switch data.productType {
        case FilterType.lights.displayName:
            mainImageView.image = #imageLiteral(resourceName: "ic_lights")
        case FilterType.rollerShutter.displayName:
            mainSwitch.isHidden = true
            mainImageView.image = #imageLiteral(resourceName: "roller_shutter")
        case FilterType.heaters.displayName:
            mainImageView.image = #imageLiteral(resourceName: "heaters")
        default:
            mainImageView.image = #imageLiteral(resourceName: "ic_lights")
        }

        nameLabel.text = data.deviceName
    }
    @IBAction private func didChangeValue(_ sender: UISwitch) {
        self.didChangeMode?(sender.isOn)
    }

    @IBAction private func tapDeleteButtonAction(_ sender: UIButton) {
        self.didSelectRemove?()
    }

}
