//
//  HomePageContentCell.swift
//  ModuloTech
//
//  Created by HieuH on 16/06/2021.
//

import UIKit

protocol HomePageContentDelegate: NSObjectProtocol {
    func didRemoveItem(at index: Int)
    func didChangeModeOfItem(id: Int64?, isOn: Bool)
}

class HomePageContentCell: UITableViewCell {
    @IBOutlet private weak var mainCollectionView: UICollectionView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var heightOfCollectionViewConstraint: NSLayoutConstraint!

    var devices: [Device] = []
    let identifier = "DeviceInforCell"
    var didTapOnItem: ((_ id: Int) -> Void)?
    weak var delegate: HomePageContentDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()

        selectionStyle = .none
        setupCollectionView()
        titleLabel.text = Localization.string(.your_using_device_title)
    }

    func configData(data: [Device], didTapOnItem: ((_ id: Int) -> Void)?) {
        self.didTapOnItem = didTapOnItem
        self.devices = data

        mainCollectionView.reloadData()
        heightOfCollectionViewConstraint.constant = mainCollectionView.collectionViewLayout.collectionViewContentSize.height
        self.layoutIfNeeded()
    }

    func setupCollectionView() {
        let cellNib = UINib(nibName: identifier, bundle: nil)
        mainCollectionView.register(cellNib, forCellWithReuseIdentifier: identifier)

        mainCollectionView.dataSource = self
        mainCollectionView.delegate = self

    }
}

extension HomePageContentCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return devices.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! DeviceInforCell
        cell.setupData(data: devices[indexPath.item], didSelectRemove: { [weak self] in
            guard let self = self else {
                return
            }
            self.delegate?.didRemoveItem(at: indexPath.item)
        }, didChangeMode: { [weak self] isOn in
            guard let self = self else {
                return
            }
            self.delegate?.didChangeModeOfItem(id: self.devices[indexPath.item].id, isOn: isOn)
        })
        return cell
    }

}

extension HomePageContentCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if !devices.isEmpty {
            self.didTapOnItem?(indexPath.item)
        }
    }
}

extension HomePageContentCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 24
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.width - 64) / 2, height: 180)
    }
}
