//
//  HomePageHeaderViewCell.swift
//  ModuloTech
//
//  Created by HieuH on 16/06/2021.
//

import UIKit

enum FilterType: Int {
    case all
    case lights
    case rollerShutter
    case heaters

    var displayName: String {
        switch self {
        case .all:
            return "All"
        case .lights:
            return "Light"
        case .rollerShutter:
            return "RollerShutter"
        case .heaters:
            return "Heater"
        }
    }
}

protocol HomePageHeaderDelegate: NSObjectProtocol {
    func didTapFilter(type: FilterType)
    func didTapOnAvatar()
}

class HomePageHeaderCell: UITableViewCell {
    @IBOutlet private weak var introLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var lightsView: UIView!
    @IBOutlet private weak var rollersShutterView: UIView!
    @IBOutlet private weak var heatersView: UIView!
    @IBOutlet private weak var lightsLabel: UILabel!
    @IBOutlet private weak var rollerShutterLabel: UILabel!
    @IBOutlet private weak var heatersLabel: UILabel!
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var allView: UIView!
    @IBOutlet private weak var allLabel: UILabel!
    @IBOutlet private weak var allSecondsLabel: UILabel!

    weak var delegate: HomePageHeaderDelegate?
    var currentFilterType: FilterType = .all

    override func awakeFromNib() {
        super.awakeFromNib()

        selectionStyle = .none
        updateButton()
        setupLayout()
        addTapGuesture()
        setupBaseData()
    }

    func setupLayout() {
        lightsView.layer.cornerRadius = 16
        rollersShutterView.layer.cornerRadius = 16
        heatersView.layer.cornerRadius = 16
        allView.layer.cornerRadius = 16

        avatarImageView.isUserInteractionEnabled = true
    }
    
    func updateButton() {
        allView.backgroundColor = UIColor.init(hexString: "#8B8B8B")
        lightsView.backgroundColor = UIColor.init(hexString: "#8B8B8B")
        rollersShutterView.backgroundColor = UIColor.init(hexString: "#8B8B8B")
        heatersView.backgroundColor = UIColor.init(hexString: "#8B8B8B")
        
        switch currentFilterType {
        case .all:
            allView.backgroundColor = .green
        case .lights:
            lightsView.backgroundColor = .green
        case .rollerShutter:
            rollersShutterView.backgroundColor = .green
        case .heaters:
            heatersView.backgroundColor = .green
        }
    }

    func addTapGuesture() {
        let tapAll = UITapGestureRecognizer(target: self, action: #selector(self.handleTapAll(_:)))
        allView.addGestureRecognizer(tapAll)

        let tapLights = UITapGestureRecognizer(target: self, action: #selector(self.handleTapLights(_:)))
        lightsView.addGestureRecognizer(tapLights)

        let tapRollerShutter = UITapGestureRecognizer(target: self, action: #selector(self.handleTapRollerShutter(_:)))
        rollersShutterView.addGestureRecognizer(tapRollerShutter)

        let tapHeaters = UITapGestureRecognizer(target: self, action: #selector(self.handleTapHeaters(_:)))
        heatersView.addGestureRecognizer(tapHeaters)

        let tapOnAvatar = UITapGestureRecognizer(target: self, action: #selector(self.handleTapOnAvatar(_:)))
        avatarImageView.addGestureRecognizer(tapOnAvatar)
    }

    @objc
    func handleTapAll(_ sender: UITapGestureRecognizer?) {
        if currentFilterType != .all {
            delegate?.didTapFilter(type: .all)
            currentFilterType = .all
            updateButton()
        }
    }

    @objc
    func handleTapLights(_ sender: UITapGestureRecognizer?) {
        if currentFilterType != .lights {
            delegate?.didTapFilter(type: .lights)
            currentFilterType = .lights
            updateButton()

        }
    }

    @objc
    func handleTapRollerShutter(_ sender: UITapGestureRecognizer?) {
        if currentFilterType != .rollerShutter {
            delegate?.didTapFilter(type: .rollerShutter)
            currentFilterType = .rollerShutter
            updateButton()
        }
    }

    @objc
    func handleTapHeaters(_ sender: UITapGestureRecognizer?) {
        if currentFilterType != .heaters {
            delegate?.didTapFilter(type: .heaters)
            currentFilterType = .heaters
            updateButton()
        }
    }

    @objc
    func handleTapOnAvatar(_ sender: UITapGestureRecognizer?) {
        delegate?.didTapOnAvatar()
    }

    func setupBaseData() {
        allLabel.text = Localization.string(.all_title)
        allSecondsLabel.text = ""
        descriptionLabel.text = Localization.string(.intro_title)
        lightsLabel.text =  Localization.string(.lights_title)
        rollerShutterLabel.text = Localization.string(.roller_shutter_title)
        heatersLabel.text = Localization.string(.heaters_title)
    }

    func configData(name: String) {
        introLabel.text = "\(Localization.string(.hi_title)), \(name)"
    }
}
