//
//  ViewController.swift
//  ModuloTech
//
//  Created by HieuH on 16/06/2021.
//

import UIKit

class HomePageViewController: UIViewController {
    let headerIdentify = "HomePageHeaderCell"
    let contentIdentify = "HomePageContentCell"
    var mainTableView = UITableView()
    var filterDevices: [Device] = []
    var isNeedReload: Bool = false
    let pointer = APIService()
    var baseInfomation: BaseInformation?

    private var homePageViewModel : HomePageViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(self.didUpdateData(_:)), name: NSNotification.Name(rawValue: "didUpdateDevice"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.didUpdateData(_:)), name: NSNotification.Name(rawValue: "didUpdateInfor"), object: nil)

        addTableView()
        callToViewModelForUIUpdate()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @objc
    func didUpdateData(_ notification: NSNotification) {
        if let dict = notification.userInfo as NSDictionary? {
            if let id = dict["id"] as? Int64 {
                guard let index = self.filterDevices.firstIndex(where: { $0.id == id }) else {
                    return
                }
                guard let parentIndex = baseInfomation?.devices?.firstIndex(where: { $0.id == id }) else {
                    return
                }
                if let isON = dict["mode"] as? Bool {
                    self.filterDevices[index].mode = isON ? "ON" : "OFF"
                    self.baseInfomation?.devices?[parentIndex].mode = isON ? "ON" : "OFF"
                }
                if let intensity = dict["intensity"] as? Int {
                    self.filterDevices[index].intensity = Int64(intensity)
                    self.baseInfomation?.devices?[parentIndex].intensity = Int64(intensity)
                }
                if let temperature = dict["temperature"] as? String {
                    self.filterDevices[index].temperature = Int64(temperature)
                    self.baseInfomation?.devices?[parentIndex].temperature = Int64(temperature)
                }
                if let position = dict["position"] as? Float {
                    self.filterDevices[index].position = Int64(position)
                    self.baseInfomation?.devices?[parentIndex].position = Int64(position)
                }
            }
            if let user = dict["newUserInfor"] as? UserInfor {
                self.baseInfomation?.user = user
            }
        }
        mainTableView.reloadData()
     }
    
    func callToViewModelForUIUpdate(){
        homePageViewModel = HomePageViewModel()
        homePageViewModel.bindHomePageViewModelToController = { data in
            self.updateDataSource(data: data)
        }
    }

    func updateDataSource(data: BaseInformation) {
        baseInfomation = data
        filterDevices = data.devices ?? []
        DispatchQueue.main.async {
            self.mainTableView.reloadData()
        }
    }

    func addTableView() {
        let headerNib = UINib(nibName: headerIdentify, bundle: nil)
        let contentNib = UINib(nibName: contentIdentify, bundle: nil)

        mainTableView = UITableView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        mainTableView.backgroundColor = UIColor.init(hexString: "#E5E5E5")
        mainTableView.separatorStyle = .none
        mainTableView.register(headerNib, forCellReuseIdentifier: headerIdentify)
        mainTableView.register(contentNib, forCellReuseIdentifier: contentIdentify)

        mainTableView.delegate = self
        mainTableView.dataSource = self

        self.view.addSubview(mainTableView)

    }

    func navigateToDeviceDetail(data: Device) {
        let viewController = DetailDeviceViewController()
        viewController.device = data
        viewController.modalTransitionStyle = .coverVertical
        viewController.modalPresentationStyle = .overFullScreen
        self.present(viewController, animated: true, completion: nil)
    }

    func navigateTopProfile() {
        let viewController = ProfileDetailViewController()
        viewController.userInfor = baseInfomation?.user
        viewController.modalTransitionStyle = .coverVertical
        viewController.modalPresentationStyle = .overFullScreen
        self.present(viewController, animated: true, completion: nil)
    }

    func showPopupConfirm(index: Int) {
        let viewController = PopupCommonViewController()
        viewController.alertType = .confirm
        viewController.message = Localization.string(.confirm_delete_msg)
        viewController.modalTransitionStyle = .coverVertical
        viewController.modalPresentationStyle = .overFullScreen

        viewController.didChooseAgree = { [weak self] in
            guard let self = self else {
                return
            }
            self.filterDevices.remove(at: index)
            self.baseInfomation?.devices?.remove(at: index)
            self.mainTableView.reloadData()
        }
        self.present(viewController, animated: true, completion: nil)
    }
}

extension HomePageViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell: HomePageHeaderCell = tableView.dequeueReusableCell(withIdentifier: headerIdentify, for: indexPath) as! HomePageHeaderCell
            cell.configData(name: baseInfomation?.user.firstName ?? "")
            cell.delegate = self
            return cell
        default:
            let cell: HomePageContentCell = tableView.dequeueReusableCell(withIdentifier: contentIdentify, for: indexPath) as! HomePageContentCell
            cell.configData(data: filterDevices) { [weak self] index in
                guard let self = self else {
                    return
                }
                self.navigateToDeviceDetail(data: self.filterDevices[index])
            }
            cell.delegate = self
            return cell
        }
    }
}

extension HomePageViewController: HomePageHeaderDelegate {

    func didTapFilter(type: FilterType) {
        if let baseInfor = baseInfomation {
            switch type {
            case .all:
                filterDevices = baseInfor.devices ?? []
            case .lights:
                filterDevices = baseInfor.devices?.filter({ $0.productType == FilterType.lights.displayName }) ?? []
            case .rollerShutter:
                filterDevices = baseInfor.devices?.filter({ $0.productType == FilterType.rollerShutter.displayName }) ?? []
            case .heaters:
                filterDevices = baseInfor.devices?.filter({ $0.productType == FilterType.heaters.displayName }) ?? []
            }
        }

        mainTableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .none)
    }

    func didTapOnAvatar() {
        navigateTopProfile()
    }
}

extension HomePageViewController: HomePageContentDelegate {
    func didChangeModeOfItem(id: Int64?, isOn: Bool) {
        guard let index = filterDevices.firstIndex(where: { $0.id == id }) else {
            return
        }
        filterDevices[index].mode = isOn ? "ON" : "OFF"
        guard let parentIndex = baseInfomation?.devices?.firstIndex(where: { $0.id == id }) else {
            return
        }
        baseInfomation?.devices?[parentIndex].mode = isOn ? "ON" : "OFF"
    }

    func didRemoveItem(at index: Int) {
        self.showPopupConfirm(index: index)
    }
}
