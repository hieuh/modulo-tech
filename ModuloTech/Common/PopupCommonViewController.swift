//
//  PopupCommonViewController.swift
//  ModuloTech
//
//  Created by HieuH on 20/06/2021.
//

import UIKit

enum AlertType: Int {
    case confirm
    case warning

    var displayType: String {
        switch self {
        case .confirm:
            return Localization.string(.confirm_title)
        case .warning:
            return "Opps"
        }
    }
}

class PopupCommonViewController: UIViewController {

    var message: String?
    var alertType: AlertType = .confirm
    var didChooseAgree: (() -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .black.withAlphaComponent(0.8)
        let gesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapCancel))
        gesture.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(gesture)
        addChildItem()
    }

    func addChildItem() {
        let mainView = UIView()
        mainView.layer.cornerRadius = 16
        mainView.backgroundColor = .white
        mainView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(mainView)

        mainView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        mainView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 24).isActive = true
        mainView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -24).isActive = true

        let iconImageView = UIImageView()
        switch self.alertType {
        case .confirm:
            iconImageView.image = #imageLiteral(resourceName: "ic_confirm")
        case .warning:
            iconImageView.image = #imageLiteral(resourceName: "ic_warning")
        }
        iconImageView.translatesAutoresizingMaskIntoConstraints = false
        mainView.addSubview(iconImageView)

        iconImageView.topAnchor.constraint(equalTo: mainView.topAnchor, constant: 4).isActive = true
        iconImageView.centerXAnchor.constraint(equalTo: mainView.centerXAnchor).isActive = true
        iconImageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        iconImageView.heightAnchor.constraint(equalToConstant: 50).isActive = true

        let mainLabel = UILabel()
        mainLabel.text = message
        mainLabel.textAlignment = .center
        mainLabel.numberOfLines = 0
        mainLabel.translatesAutoresizingMaskIntoConstraints = false
        mainView.addSubview(mainLabel)

        mainLabel.centerXAnchor.constraint(equalTo: mainView.centerXAnchor).isActive = true
        mainLabel.topAnchor.constraint(equalTo: iconImageView.bottomAnchor, constant: 16).isActive = true
        mainLabel.leadingAnchor.constraint(equalTo: mainView.leadingAnchor, constant: 24).isActive = true
        mainLabel.trailingAnchor.constraint(equalTo: mainView.trailingAnchor, constant: -24).isActive = true

        let mainStackView = UIStackView()
        mainStackView.axis = .horizontal
        mainStackView.spacing = 24
        mainStackView.translatesAutoresizingMaskIntoConstraints = false
        mainView.addSubview(mainStackView)

        mainStackView.centerXAnchor.constraint(equalTo: mainView.centerXAnchor).isActive = true
        mainStackView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        mainStackView.topAnchor.constraint(equalTo: mainLabel.bottomAnchor, constant: 24).isActive = true
        mainStackView.bottomAnchor.constraint(equalTo: mainView.bottomAnchor, constant: -24).isActive = true

        let agreeButton = UIButton()
        agreeButton.translatesAutoresizingMaskIntoConstraints = false
        agreeButton.layer.cornerRadius = 8
        switch self.alertType {
        case .confirm:
            agreeButton.setTitle(alertType.displayType, for: .normal)
        case .warning:
            agreeButton.setTitle("OK", for: .normal)
        }
        agreeButton.setTitleColor(.white, for: .normal)
        agreeButton.backgroundColor = UIColor.init(hexString: "#185ECE")
        agreeButton.addTarget(self, action: #selector(tapAgree), for: .touchUpInside)
        agreeButton.widthAnchor.constraint(equalToConstant: 120).isActive = true

        let cancelButton = UIButton()
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        cancelButton.layer.cornerRadius = 8
        cancelButton.setTitle("Cancel", for: .normal)
        cancelButton.setTitleColor(.white, for: .normal)
        cancelButton.backgroundColor = UIColor.init(hexString: "#185ECE").withAlphaComponent(0.6)
        cancelButton.addTarget(self, action: #selector(tapCancel), for: .touchUpInside)
        cancelButton.widthAnchor.constraint(equalToConstant: 120).isActive = true

        mainStackView.addArrangedSubview(cancelButton)
        mainStackView.addArrangedSubview(agreeButton)

        cancelButton.isHidden = alertType == .warning
    }

    @objc
    func tapAgree() {
        didChooseAgree?()
        self.dismiss(animated: true, completion: nil)
    }

    @objc
    func tapCancel() {
        self.dismiss(animated: true, completion: nil)
    }
}
