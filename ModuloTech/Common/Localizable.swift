//
//  Localizableswift
//  ModuloTech
//
//  Created by HieuH on 18/06/2021
//

import Foundation

enum LocalStrings: String {
    case your_using_device_title
    case intensity_title
    case position_title
    case temperature_title
    case edit_profile_title
    case date_of_birthday_title
    case first_name_title
    case last_name_title
    case email_address_title
    case street_title
    case street_code_title
    case city_title
    case postal_code_title
    case country_title
    case save_changes_success_msg
    case save_changes_title
    case intro_title
    case all_title
    case hi_title
    case lights_title
    case roller_shutter_title
    case heaters_title
    case mode_title
    case delete_device_msg
    case done_title
    case email_not_correct_msg
    case can_not_empty_msg
    case confirm_delete_msg
    case confirm_title
}

class Localization {
    class func string(_ string: LocalStrings) -> String {
        return NSLocalizedString(string.rawValue, comment: "")
    }
}

