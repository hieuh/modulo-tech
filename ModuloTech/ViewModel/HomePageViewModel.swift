//
//  HomePageViewModel.swift
//  ModuloTech
//
//  Created by HieuH on 16/06/2021.
//

import Foundation

class HomePageViewModel: NSObject {

    private var apiService = APIService()
    private(set) var empData: BaseInformation! {
        didSet {
            self.bindHomePageViewModelToController(empData)
        }
    }
    var bindHomePageViewModelToController : ((_ data: BaseInformation) -> Void) = { _ in }

    override init() {
        super.init()
        getDataFromAPI()
    }

    func getDataFromAPI() {
        apiService.apiToGetUserData { (data) in
            self.empData = data
        }
    }
}
