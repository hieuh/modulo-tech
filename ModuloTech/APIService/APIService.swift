//
//  APIService.swift
//  ModuloTech
//
//  Created by HieuH on 18/06/2021.
//

import Foundation
import UIKit

class APIService {
    private let sourcesURL = URL(string: "http://storage42.com/modulotest/data.json")!

    func apiToGetUserData(completion : @escaping (BaseInformation) -> ()){
        URLSession.shared.dataTask(with: sourcesURL) { (data, urlResponse, error) in
            if let data = data {
                let jsonDecoder = JSONDecoder()
                let empData = try! jsonDecoder.decode(BaseInformation.self, from: data)
                completion(empData)
            }
        }.resume()
    }
}
