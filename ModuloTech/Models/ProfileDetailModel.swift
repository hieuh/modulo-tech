//
//  ProfileDetailModel.swift
//  ModuloTech
//
//  Created by HieuH on 17/06/2021.
//

import Foundation

enum ProfileDetailType: Int, CaseIterable {
    case firstName
    case email
    case dateOfBirth
    case street
    case city
    case country

    var displayName: String {
        switch self {
        case .firstName:
            return Localization.string(.first_name_title)
        case .email:
            return Localization.string(.email_address_title)
        case .dateOfBirth:
            return Localization.string(.date_of_birthday_title)
        case .street:
            return Localization.string(.street_title)
        case .city:
            return Localization.string(.city_title)
        case .country:
            return Localization.string(.country_title)
        }
    }
}

enum ChildProfileType: Int, CaseIterable {
    case lastName
    case streetCode
    case postalCode

    var displayName: String {
        switch self {
        case .lastName:
            return Localization.string(.last_name_title)
        case .streetCode:
            return Localization.string(.street_code_title)
        case .postalCode:
            return Localization.string(.postal_code_title)
        }
    }
}
