//
//  HomePageModel.swift
//  ModuloTech
//
//  Created by HieuH on 16/06/2021.
//

import Foundation

struct BaseInformation: Decodable {
    var devices: [Device]?
    var user: UserInfor
}

struct Device: Decodable {
    var id: Int64?
    var deviceName: String?
    var mode: String?
    var intensity: Int64?
    var temperature: Int64?
    var position: Int64?
    var productType: String?

    init(id: Int64?, deviceName: String?, mode: String?, intensity: Int64?, temperature: Int64?, position: Int64?, productType: String?) {
        self.id = id
        self.deviceName = deviceName
        self.mode = mode
        self.intensity = intensity
        self.temperature = temperature
        self.position = position
        self.productType = productType
    }
}

struct UserInfor: Decodable {
    var firstName: String?
    var lastName: String?
    var email: String?
    var birthDate: Int64?
    var address: UserAddress?

    init(firstName: String?, lastName: String?, email: String?, birthDate: Int64?, userAddress: UserAddress?) {
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.birthDate = birthDate
        self.address = userAddress
    }
}

struct UserAddress: Decodable {
    var city: String?
    var postalCode: Int64?
    var street: String?
    var streetCode: String?
    var country: String?

    init(city: String?, postalCode: Int64?, street: String?, streetCode: String?, country: String?) {
        self.city = city
        self.postalCode = postalCode
        self.street = street
        self.streetCode = streetCode
        self.country = country
    }
}
